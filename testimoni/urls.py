from django.urls import path
from . import views

app_name="testimoni"
urlpatterns = [
    path('isitesti/', views.testimoni, name='testimoni'),
    path('gettesti/', views.PostListView().as_view(), name='gettestimoni'),
    path('create/', views.PostCreateView.as_view(), name=None),
    path('<int:pk>/', views.PostDetailView.as_view(), name=None),
]

# PostListView.as_view()