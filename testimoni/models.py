from django.db import models

# Create your models here.
class Testimoni(models.Model):
    nama = models.CharField(max_length = 10)
    pekerjaan = models.CharField(max_length = 10)
    testi = models.CharField(max_length = 100)
    waktu = models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False)
