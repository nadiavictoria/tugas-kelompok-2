from django.shortcuts import render,redirect
from .models import Testimoni
from .forms import TestimoniForm
from django.contrib.auth.decorators import login_required

from .api import serializers
from rest_framework import generics, status
from rest_framework.response import Response
from django.http import JsonResponse

# Create your views here.
class PostListView(generics.ListAPIView):
    queryset = Testimoni.objects.all()
    serializer_class = serializers.PostSerializer

class PostCreateView(generics.CreateAPIView):
    queryset = Testimoni.objects.all()
    serializer_class = serializers.PostSerializer

    def create(self, request, *args, **kwargs):
        super(PostCreateView, self).create(request, args, kwargs)
        response = {"status_code": status.HTTP_200_OK,
                    "message": "Successfully created",
                    "result": request.data}
        return Response(response)

class PostDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Testimoni.objects.all()
    serializer_class = serializers.PostSerializer

    def retrieve(self, request, *args, **kwargs):
        super(PostDetailView, self).retrieve(request, args, kwargs)
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        data = serializer.data
        response = {"status_code": status.HTTP_200_OK,
                    "message": "Successfully retrieved",
                    "result": data}
        return Response(response)

    def patch(self, request, *args, **kwargs):
        super(PostDetailView, self).patch(request, args, kwargs)
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        data = serializer.data
        response = {"status_code": status.HTTP_200_OK,
                    "message": "Successfully updated",
                    "result": data}
        return Response(response)

    def delete(self, request, *args, **kwargs):
        super(PostDetailView, self).delete(request, args, kwargs)
        response = {"status_code": status.HTTP_200_OK,
                    "message": "Successfully deleted"}
        return Response(response)

# @login_required
def testimoni(request):
    if request.method == "POST":
        form = TestimoniForm(request.POST)
        if form.is_valid():
            testi = form.save(commit=False)
            testi.save()
            return redirect('testimoni:testimoni')
    else:
        form = TestimoniForm()
        testi = Testimoni.objects.all().order_by('-waktu')
    return render(request, 'testimoni.html', {'form' : form, 'testi': testi})