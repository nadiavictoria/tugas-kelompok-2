from ..models import Testimoni
from rest_framework import serializers

class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Testimoni
        fields = ('nama','pekerjaan', 'testi', 'waktu') # if not declare