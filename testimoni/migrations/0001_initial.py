# Generated by Django 3.0 on 2019-12-06 07:25

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Testimoni',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(default='Unknown', max_length=10)),
                ('testi', models.CharField(default='Unknown', max_length=100)),
            ],
        ),
    ]
