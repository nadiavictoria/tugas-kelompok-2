from django.test import TestCase, LiveServerTestCase
from .models import Testimoni
from .views import testimoni
from .forms import TestimoniForm
from django.urls import resolve
from django.contrib.auth.models import User

#from selenium import webdriver
#from selenium.webdriver.common.keys import Keys
#from selenium.webdriver.chrome.options import Options


# Create your tests here.
class TestTestimoni(TestCase):

    def test_landing(self):
        response = self.client.get("/testimoni/isitesti/")
        self.assertTemplateUsed(response, 'testimoni.html')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response,'<form')

    def test_landing_views(self):
        response = resolve("/testimoni/isitesti/")
        self.assertEqual(response.func, testimoni)

    def create_testimoni(self):
        new_testimoni = Testimoni.objects.create(nama = "joni", pekerjaan = "programmer", testi = "capek euy")
        return new_testimoni

    def test_check_testimoni(self):
        c = self.create_testimoni()
        self.assertTrue(isinstance(c, Testimoni))
        self.assertTrue(c.__str__(), c.nama)
        counting_all_status = Testimoni.objects.all().count()
        self.assertEqual(counting_all_status, 1)
    
    def test_form(self):
        data_form = {
            'nama' : 'elton jhonu',
            'pekerjaan' : 'codestealer',
            'testi' : 'aku plagiator haha',
        }
        form = TestimoniForm(data = data_form)
        self.assertFalse(form.is_valid())
        # request = self.client.post('/testimoni/isitesti/', data_form)
        # self.assertEqual(request.status_code, 302)

        # response = self.client.get('/testimoni/isitesti/')
        # self.assertEqual(response.status_code, 200)
