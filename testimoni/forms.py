from django import forms
from .models import Testimoni
from django.forms import widgets

class TestimoniForm(forms.ModelForm):
    class Meta():
        model = Testimoni
        fields = {'nama','pekerjaan','testi'}
        widgets = {
            'nama' : forms.Textarea(attrs={'placeholder': 'Elton John...', 'cols':7, 'rows': 1}),
            'pekerjaan' : forms.Textarea(attrs={'placeholder': 'Pencurhat...', 'cols':7, 'rows': 1}),
            'testi' : forms.Textarea(attrs={'placeholder': 'Saya suka curhatMe...', 'cols':7, 'rows': 5}),
        }

    def __init__(self, *args, **kwargs):
        super(TestimoniForm,self).__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })