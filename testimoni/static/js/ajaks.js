$(document).on({
    ajaxStart: function () {
        $('.none').css('display', 'block');
    },
    ajaxStop: function () {
        $('.none').css('display', 'none');
    }
});

$(document).ready(function (){
    $('#tampiltesti').click(function () {
        if($('#tampiltesti').html() == 'Tampilkan Testimoni'){
            $('#tampiltesti').html('Tutup Testimoni');
            let databesItem = "/testimoni/gettesti/";

            $.ajax({
                method: "GET",
                url: databesItem,
                success: function(dataRes){
                    console.log(dataRes)
                    for(i = dataRes.length-1; i >= 0; i--){
                        $("body").append(
                            '<div class="col-md-8 offset-2 thegridbottom card" id="hasilprint"><div class="card-body"><div><div>' 
                            + "Nama : " + dataRes[i].nama + '<br>' 
                            + "Pekerjaan : " + dataRes[i].pekerjaan + '<br>'
                            + "Testimoni : " + dataRes[i].testi + '<br>'
                            + "|| Posted on : " + dataRes[i].waktu + ' ||' + '<br>'
                            +
                            '</div></div></div></div>');
                        }
                    },
                    error: function(error_dataRes){
                        console.log("error");
                    }
                });
        }
        else{
            $('#tampiltesti').html('Tampilkan Testimoni');
            let databesItem = "/testimoni/gettesti/";
            $.ajax({
                method: "GET",
                url: databesItem,
                success: function(dataRes){
                    console.log(dataRes)
                    for(i = 0; i < dataRes.length; i++){
                        $("#hasilprint").remove();}
                    },
                    error: function(error_dataRes){
                        console.log("error");
                    }
                });
        }
    });
});