from .models import storyCurhat
from .forms import CurhatForm
from . import serializers
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.generics import ListAPIView
from django.shortcuts import render, redirect

class storyCurhatListView(generics.ListAPIView):
    queryset = storyCurhat.objects.all()
    serializer_class = serializers.storyCurhatSerializer

def timeline(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = CurhatForm(request.POST)
            if form.is_valid():
                form.save()
                return redirect('thepost:timeline')
        
        else:
            timeline = {'formCurhat': CurhatForm(), 'listCurhat': storyCurhat.objects.all()}
        return render(request, 'timeline.html', timeline)
    else:
        return render(request, 'login.html')

def search(request):
    return render(request, 'search.html')
