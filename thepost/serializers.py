from .models import storyCurhat
from rest_framework import serializers

class storyCurhatSerializer(serializers.ModelSerializer):
    class Meta:
        model = storyCurhat
        fields = ('story', 'tags')