from django.urls import path
from .views import *

app_name = 'thepost'

urlpatterns = [
    path('', timeline, name='timeline'),
    path('search/', search, name='search'),
    path('api/', storyCurhatListView.as_view(), name=None)
]