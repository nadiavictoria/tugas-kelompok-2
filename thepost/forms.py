from django import forms
from .models import storyCurhat

class CurhatForm(forms.ModelForm):
    story = forms.CharField(max_length=200, label='Ada Cerita apa hari ini?', widget=forms.Textarea(attrs={'class':'form-control', 'required': True, }))
    tags = forms.CharField(max_length=15, label='Tags', widget=forms.TextInput(attrs={'class':'form-control', 'required': True, }))

    class Meta:
        model = storyCurhat
        fields = ['story', 'tags']