from django.db import models

class storyCurhat(models.Model):
    story = models.CharField(max_length=200, default='Curhat')
    tags = models.CharField(max_length=15, default='Tags')

    def __str__(self):
        return self.story