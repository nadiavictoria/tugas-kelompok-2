from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from django.http import HttpRequest
import time

from .views import timeline
from .models import storyCurhat
from .forms import CurhatForm

# from selenium import webdriver
#from selenium.webdriver.common.keys import Keys
#from selenium.webdriver.chrome.options import Options

class thepostUnitTest(TestCase):

    def test_thepost_url_is_exist(self):
        response = Client().get('/thepost/')
        self.assertEqual(response.status_code, 200)
    
    def test_create_curhat(self):
        new_status = storyCurhat.objects.create(story = "Test Status")
        return new_status
    
    def test_check_status(self):
        check = self.test_create_curhat()
        self.assertTrue(isinstance(check, storyCurhat))
        self.assertTrue(check.__str__(), check.story)
        count_all_status = storyCurhat.objects.all().count()
        self.assertEqual(count_all_status, 1)
    
    def test_form_CurhatForm(self):
        test_data = {
            'story' : 'statusku masih single :)',
            'tags' : 'Jomblo'
        }
        form = CurhatForm(data = test_data)
        self.assertTrue(form.is_valid())

        request = self.client.post('/thepost/', data = test_data)
        self.assertEqual(request.status_code, 200)

        response = self.client.get('/thepost/')
        self.assertEqual(response.status_code, 200)
    
    def test_search_url_is_exist(self):
        response = Client().get('/thepost/search/')
        self.assertEqual(response.status_code, 200)
    
    # def test_template_search(self):
    #     found = resolve('/thepost/search/')
    #     self.assertTemplateUsed(response, search.html)


# class ThePostFunctionalTest(LiveServerTestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')        
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('--disable-gpu')
#         chrome_options.add_argument('--disable-dev-shm-usage')
#         self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
    
#     def tearDown(self):
#         self.browser.quit()
    
#     def test_input_status_from_user(self):
#         time.sleep(3)
#         self.browser.get(self.live_server_url+"/thepost/")
#         time.sleep(3)
        
#         # Test if HTML appear a sentences "Ada cerita apa hari ini?"
#         self.assertInHTML("Ada cerita apa hari ini?", self.browser.page_source)

#         # Test status and the submit button in Landing Page
#         status = self.browser.find_element_by_id('labelreply')
#         submit = self.browser.find_element_by_id('balas')

#         status.send_keys("Test")
#         submit.send_keys(Keys.ENTER)