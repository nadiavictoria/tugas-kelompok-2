from django.urls import path
from . import views

app_name = 'authentication'

urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('register/', views.register, name='register'),
    path('login/', views.login_request, name='login'),
    path('api/', views.UserListView.as_view(), name=None),
    path('logout/', views.logout_request, name='logout'),
]