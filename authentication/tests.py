from django.test import TestCase
from django.test import Client
from django.urls import resolve

from .views import homepage, register, login_request
from django.contrib.auth.models import User

class TK2UnitTest(TestCase):
    def test_homepage_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_homepage_func(self):
        found = resolve('/')
        self.assertEqual(found.func, homepage)

    def test_login_url_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)
        
    def test_regis_url_is_exist(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)

class LogInRegisTest(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'testuser',
            'password': 'secretpass'}
        User.objects.create_user(**self.credentials)

    def test_register(self):
        # login
        response = self.client.post('/register/', **self.credentials)      
        # should be logged in now, fails however
        self.assertFalse(response.context['user'].is_active)

    def test_login(self):
        # login
        response = self.client.post('/login/', **self.credentials)      
        # should be logged in now, fails however
        self.assertFalse(response.context['user'].is_active)