from django.shortcuts import render, redirect
from .models import Reply
from .import forms
from .import serializers
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.generics import ListAPIView

def replies(request):
    return render(request, 'postreply.html')

def reply(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = forms.ReplyForm(request.POST)
            if form.is_valid():
                form.save()
                return redirect('thereply:replies')

        else:
            form = forms.ReplyForm()
        return render(request, 'reply.html', {'form': form})

    else:
        return render(request, 'login.html')


class ReplyListView(generics.ListAPIView):
    queryset = Reply.objects.all()
    serializer_class = serializers.ReplySerializer