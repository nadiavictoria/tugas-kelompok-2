from django.urls import path
from . import views

app_name = 'thereply'

urlpatterns = [
    path('', views.reply, name='reply'),
    path('postreply/', views.ReplyListView.as_view(), name='postreply'),
    path('replies/', views.replies, name='replies'),
]