from django.test import TestCase
from django.urls import resolve
from django.test.client import Client
from .views import *
from .models import *

# Create your tests here.
class UnitTestCurhatMe(TestCase):
    #tes url ada apa ga
    def test_homepage_url_is_exist(self):
        response = Client().get('/thereply/')
        self.assertEqual(response.status_code, 200)

    #tes url reply manggil fungsi apa
    def test_using_index_func(self):
        found = resolve('/thereply/')
        self.assertEqual(found.func, reply)

    #tes url reply manggil fungsi apa
    def test_using_index_func2(self):
        found = resolve('/thereply/replies/')
        self.assertEqual(found.func, replies)

    #tes url reply manggil html apa
    def test_using_to_do_list_template(self):
        response = Client().get('/thereply/')
        self.assertTemplateUsed(response, 'login.html')

    # # cek bisa bikin model apa ga, trus tes jumlahnya
    # def test_model_can_create_new_reply(self):
    #     new_reply = Reply.objects.create(reply='Aku capek')

    #     counting_all_available_reply = Reply.objects.all().count()
    #     self.assertEqual(counting_all_available_reply, 1)

    # #tes model bisa bikin model apa ga
    # def test_model_check_create_new_reply(self):
    #     reply = {'reply':"aku capek"}
    #     response = Client().post('/thereply/', reply)
    #     self.assertEqual(response.status_code, 302)

    # tes url yang gaada
    def test_urls_does_not_exist(self):
        response = Client().get('/aaaaaaa/')
        self.assertEqual(response.status_code, 404)

    # #gatau sih
    # def test_contain_name (self):
    #     response = Client().get('/thereply/')
    #     html = response.content.decode('utf8')
    #     self.assertIn("reply",html)