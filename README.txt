Anggota kelompok
1. Nadia Victoria Aritonang (1806235901) membuat fitur Login (Fitur Mandatorynya) dan Register
2. Rania Azzahra (1806185475) membuat fitur Reply
3. Muhammad Akbar Rafsanjhani (1806186686) membuat fitur Testimoni
4. Muhammad Aditya (1806147054) membuat fitur Post

Code Coverage: 
[![pipeline status](https://gitlab.com/nadiavictoria/tugas-kelompok-2/badges/master/pipeline.svg)](https://gitlab.com/nadiavictoria/tugas-kelompok-2/commits/master)

Pipeline Status: 
[![coverage report](https://gitlab.com/nadiavictoria/tugas-kelompok-2/badges/master/coverage.svg)](https://gitlab.com/nadiavictoria/tugas-kelompok-2/commits/master)
